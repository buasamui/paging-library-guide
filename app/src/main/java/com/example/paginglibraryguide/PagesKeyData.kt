package com.example.paginglibraryguide

import androidx.paging.PageKeyedDataSource

class PagesKeyData : PageKeyedDataSource<Int, News>() {

    override fun loadInitial(params: LoadInitialParams<Int>,
                             callback: LoadInitialCallback<Int, News>) {

    }

    override fun loadBefore(params: LoadParams<Int>,
                            callback: LoadCallback<Int, News>) {

    }

    override fun loadAfter(params: LoadParams<Int>,
                           callback: LoadCallback<Int, News>) {

    }

}
