package com.example.paginglibraryguide

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.RxPagedListBuilder
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable

class NewsListViewModel : ViewModel() {

    private val networkService = NetworkService.getService()
    var newsList: LiveData<PagedList<News>>
    private val compositeDisposable = CompositeDisposable()
    private val pageSize = 5
    private val newsDataSourceFactory: NewsDataSourceFactory

//    var rxNewsList: Observable<PagedList<News>>

    init {
        newsDataSourceFactory = NewsDataSourceFactory(compositeDisposable, networkService)
        val config = PagedList.Config.Builder()
                .setInitialLoadSizeHint(pageSize * 2)
                .setPageSize(pageSize)
                .setEnablePlaceholders(false)
                .setPrefetchDistance(2)
                .build()

        newsList = LivePagedListBuilder(
                newsDataSourceFactory,
                config).build()

//        rxNewsList = RxPagedListBuilder(newsDataSourceFactory, config).buildObservable()
    }

    fun retry() {
        newsDataSourceFactory.newsDataSource.retry()
    }

    fun listIsEmpty(): Boolean {
        return newsList.value?.isEmpty() ?: true
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}