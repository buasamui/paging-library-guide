package com.example.paginglibraryguide

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import androidx.paging.PagedList
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.Action
import io.reactivex.schedulers.Schedulers

class NewsDataSource(
        private val networkService: NetworkService,
        private val compositeDisposable: CompositeDisposable)
    : PageKeyedDataSource<Int, News>() {

    private var retryCompletable: Completable? = null

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, News>) {
        val disposable = networkService.getNews(1, params.requestedLoadSize)
                .subscribe(
                        { response ->
                            callback.onResult(response.news,
                                    null,
                                    2
                            )
                        },
                        {
                            setRetry(Action { loadInitial(params, callback) })
                        }
                )
        compositeDisposable.add(disposable)
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, News>) {
        //do nothing
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, News>) {
        val disposable = networkService.getNews(params.key, params.requestedLoadSize)
                .subscribe(
                        { response ->
                            callback.onResult(response.news,
                                    params.key + 1
                            )
                        },
                        {
                            setRetry(Action { loadAfter(params, callback) })
                        }
                )
        compositeDisposable.add(disposable)
    }

    fun retry() {
        retryCompletable?.let {
            val composite = it.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe()
            compositeDisposable.add(composite)
            composite
        }
    }

    private fun setRetry(action: Action?) {
        retryCompletable = if (action == null) null else Completable.fromAction(action)
    }

}