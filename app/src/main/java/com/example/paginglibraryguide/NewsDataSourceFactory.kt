package com.example.paginglibraryguide

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class NewsDataSourceFactory(
        private val compositeDisposable: CompositeDisposable,
        private val networkService: NetworkService
) : DataSource.Factory<Int, News>() {

    lateinit var newsDataSource: NewsDataSource

    override fun create(): DataSource<Int, News> {
        newsDataSource = NewsDataSource(networkService, compositeDisposable)
        return newsDataSource
    }

}