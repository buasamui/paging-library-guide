package com.example.paginglibraryguide

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_news_list.*

class NewsListActivity : AppCompatActivity() {

    private val viewModel = NewsListViewModel()
    private lateinit var newsListAdapter: NewsListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_list)
        initAdapter()
        initState()
    }

    private fun initAdapter() {
        newsListAdapter = NewsListAdapter { viewModel.retry() }
        recycler_view.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        recycler_view.adapter = newsListAdapter

        viewModel.newsList.observe(this, Observer {
            newsListAdapter.submitList(it)
        })

//        viewModel.rxNewsList
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe {
//                    newsListAdapter.submitList(it)
//                }
    }

    private fun initState() {
        txt_error.setOnClickListener { viewModel.retry() }

        newsListAdapter.addLoadStateListener { type, state, error ->
            progress_bar.visibility = if (viewModel.listIsEmpty() && state == PagedList.LoadState.DONE) View.VISIBLE else View.GONE
            txt_error.visibility = if (viewModel.listIsEmpty() && state == PagedList.LoadState.ERROR) View.VISIBLE else View.GONE
            if (!viewModel.listIsEmpty()) {
                newsListAdapter.setState(state)
            }
        }

//        newsListAdapter.addLoadStateListener { type, state, error ->
//            when (state) {
//                PagedList.LoadState.IDLE -> return@addLoadStateListener
//                PagedList.LoadState.LOADING -> return@addLoadStateListener
//                PagedList.LoadState.DONE -> return@addLoadStateListener
//                PagedList.LoadState.ERROR -> return@addLoadStateListener
//                PagedList.LoadState.RETRYABLE_ERROR -> return@addLoadStateListener
//            }
//            when (type) {
//                PagedList.LoadType.REFRESH -> return@addLoadStateListener
//                PagedList.LoadType.START -> return@addLoadStateListener
//                PagedList.LoadType.END -> return@addLoadStateListener
//            }
//        }

//        viewModel.getState().observe(this, Observer { state ->
//            progress_bar.visibility = if (viewModel.listIsEmpty() && state == PagedList.LoadState.LOADING) View.VISIBLE else View.GONE
//            txt_error.visibility = if (viewModel.listIsEmpty() && state == PagedList.LoadState.ERROR) View.VISIBLE else View.GONE
//            if (!viewModel.listIsEmpty()) {
//                newsListAdapter.setState(state ?: PagedList.LoadState.DONE)
//            }
//        })
    }

}